package com.example.noisereactor;

class Cell {
    public String data;
    public String noise;
    public String norm;

    Cell(String data, String noise, String norm) {
        this.data = data;
        this.noise = noise;
        this.norm = norm;
    }
}